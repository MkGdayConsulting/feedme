## Before Starting

1. Download the Node.js from https://nodejs.org/en/ note that you want to use the recommended one not the latest one. 
The version that is used is 12.18.4. When you finish the installation, please type node -version. If the correct version pops out, moves to the next step.

2. Install Cocoapods to your system by using "sudo gem install cocoapods". 

3. Download MongoDB from https://www.mongodb.com/try/download/compass it should have a visual installation package. Please remeber to select "install Mongo Compass" as well as it provides visual UI for the MongoDB.

4. Install Apache to activate your localhost.

5. Set up your localhost:8081. If you have never set up localhost you can do this by:
    a) Download MAMP
    b) Go to preference
    c) Set Your Apache port to 8081.

6. Download the latest version of XCode from https://developer.apple.com/download/more/.
7. Clone FeedMe source code from the bitbucket repository.



## FeedMe Server 

1. The path for our MongoDB server is "mongodb+srv://FeedMe:iu2ZUaBRMpJMKopW@cluster0.y4uwt.mongodb.net/FeedMe?retryWrites=true&w=majority"

2. To run the database use ' mongo "mongodb+srv://FeedMe:iu2ZUaBRMpJMKopW@cluster0.y4uwt.mongodb.net/FeedMe?retryWrites=true&w=majority" '

3. The server path is stored in FeedMe/.env , this can always be changed if needed.


## FeedMe backend

1. Go to FeedMe/

2. Use "npm install" to download all dependencies needed. This will create a node-module folder where all the dependencies will be handled there.

3. Use "npm start". The server should be ready to listen to the port 3000.

4. Please check .env file as all the variables are there, feel free to change the settings inside.


## FeedME frontend

1. Go to FRONTEND/jwt_client/

2. Use "npm install" to download all dependencies needed. This will create a node-module folder where all the dependencies will be handled there.

3. After all the dependencies are installed and there is no error, go to "ios" folder.

4. use "pod install" to set up the project pod. This will create ".xcodeworkspace" file, and "Pods" folder that will handle all of your pods dependencies.

5. Go to your XCode and run the .xcodeworkspace file.



## RESTful API

### localhost:PORT/api/admin      ---all the admin related functions  
* __GET__ ('/', show all the user)   
* __GET__ ('/getFood', show all the food stored in the database)
* __GET__ ('/showNutrition', show the nutrition list in the database)
* __POST__ ('/show', user's profile with userID) 
* __POST__ ('/store', create a new user)  
* __POST__ ('/update', update a user's info) 
* __POST__ ('/storeNutrition', add a nutrition to the database) 
* __POST__ ('/addNutritionToFood', add a nutrition to the food in the database) 
* __DELETE__ ('/delete', delete a user)  
* __DELETE__ ('/deleteFood/:foodID', delete a food using foodID)  
  
### localhost:PORT/api            ---all the login/register functions  
* __POST__ ('/register', register)  
* __POST__ ('/login', login)  
* __POST__ ('/verify', verify the phone number)
* __POST__ ('/check', check the verification with phone number and the code)
* __POST__ ('/cancel', user cancel the verification)
  
### localhost:PORT/api/user       ---all the user related functions  
* __GET__ ('/profile', check the profile of that user)  
* __GET__ ('/profile/add', add a preference)  
* __GET__ ('/list_nutrition', list user's nutrition's id, then you can use a for-loop  
and nutrition/show_id to get all information of user's preference)
* __POST__ ('/add_nutrition', add a nutrition to user's preference list)
* __POST__ ('/remove_nutrition', remove a nutrition from user's preference list)
  
### localhost:PORT/api/nutrition  ---all the nutrition related functions  
* __GET__ ('/', show all nutrition)  
* __GET__ ('/search', search a set of nutrition according to the keyword provided. The 
_keyword_ is passed through the URL parameters. E.g. /search?keyword=g means search nutrition with 
'g' in their description. The keyword is capital insensitive)
* __GET__ ('/show_id', show nutrition information with specific nutrition ID. The _id_ is passed 
through the URL parameters. E.g. /show_id?id=0 means retrieving the nutrition with _id 0)  
* __GET__ ('/show_description', show nutrition information with specific nutrition description. 
The _description_ is passed through the URL parameters. E.g. /show_description?description=Fish 
means retrieving the nutrition with Description "Fish")  
* __GET__ ('/load_icon', load nutrition icon with Icon_path, the image file will be sent back. The 
_path_ is passed through the URL parameters. E.g. /load_icon?path=fish.png means retrieving load 
the image fish.png from preference_icon folder in the res directory)

### localhost:PORT/api/food  ---all the nutrition related functions  
* __GET__ ('/', show all food infos)  
* __GET__ ('/getFoodIcon/:uniqueID', get the food icon using barcode or id of the food)  
* __GET__ ('/showFood/:barcode', show the food with the barcode that has been passed)  

---above 4 api don't require authorization  
---below 3 api can only be accessed by Admin  

* __DELETE__ ('/delete', delete nutrition with nutrition ID)  
* __POST__ ('/add', add a new nutrition)  
* __POST__ ('/update', update a nutrition's information)
* __POST__ ('/assign', assign an exist nutrition to a category)

### localhost:PORT/api/category   ---all the nutrition categories related functions
* __GET__ ('/', show all categories)
* __POST__ ('/show', show the information of one category)
* __POST__ ('/create', create a new category, admin login is required)
